# learning to govern
This repository contains the following:
- **010intermediate_typology.py**: the code to clean and merge the data folder "Typology" to a intermediate csv sheet.
- **011data_clean_typology.py**: the code for further data clean on filtered_typology010.csv. It produces
- **010intermediate_rephrase.py**: the code to clean and merge the data folder "Rephrase" to a intermediate csv sheet.
- **011data_clean_rephrase.py**: the code to clean and merge the data folder "Rephrase" to a single csv sheet.
- **012summary.py**: the code to produce basic statistics of the cleaned rephrase and typology data including a crosstab of number of codes each coder coded in each domain and a summary of the times each text is coded.
- **013splitting.py**: the code to split the data into training set and testing set.
- **Makefile**: project pipeline

## Instruction
To run the code, you'll need to go through the following steps:
1.  Clone this repo to your local address.
```sh
$ git clone https://gitlab.com/c2lab1/freygroup/learningtogovern.git
```
That creates a directory named learningtogovern in your local address, initializes a .git directory inside it, pulls down all the data for that repository, and checks out a working copy of the latest version. If you go into the new learningtogovern directory that was just created, you’ll see the project files in there, ready to be worked on or used.

2. Then download the data/ folder from https://ucdavis.app.box.com/folder/116324334300 to your local learningtogovern directory.
3. Run makefile command to produce cleaned csv , dev data and summary data.
```sh
$ make step1
```
The csv data will be produced to data/step1/ in your learningtogovern directory.\
The statistics will be printed on the screen.

## Data
- **filtered_typology.csv**: full cleaned dataset for typology data.
  - *code_date*: date of codifying
  - *coder*: coder
  - *timestamp*: post time of the rule
  - *communityID*: community ID
  - *lang*: rule language
  - *ref*: reference link
  - *text*: rule text
  - *reg_const*: regulation or constitution
  - *rule_norm_strategy*: rule, norm or strategy
  - *position_type*: position rule
  - *boundary_type*: boundary rule
  - *aggregation_type*: aggregation rule
  - *payoff_type*: payoff rule
  - *information_type*: information rule
  - *communication_type*: communication rule
  - *choice_type*: choice rule
  - *scope_type*: scope rule
  - *institution_type*: institution rule
  - *level_of_analysis*: level of analysis
  - *notes*: additional notes by coders
  - *domain*: community domain
  - *lineID*: ID for a complete rule
  - *before_after*: coding stage
  - *UID*: unique ID for each text
- **dev_typology.csv**: typology data dev file.
- **filtered_typology.csv**: intermediate data file.
- **filtered_rephrase.csv**: full cleaned dataset for typology data. Aside from the variables similar as *filtered_typology.csv*, different variables include:
  - *text_rephrase*: rephrased text based on the codebook.
- **dev_rephrase.csv**: rephrase data dev file.
- **QC_rephrase.csv**: rephrase data quality control file.
- **training_typology.csv**: typology data training set.
- **testing_typology.csv**: typology data testing set.
- **filtered_rephrase010.csv** and **filtered_typology010.csv**: intermediate data file.
