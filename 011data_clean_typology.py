import pandas as pd
import numpy as np
import csv
import os
import argparse

##parse command
parser = argparse.ArgumentParser(description='data clean')
parser.add_argument('-i',action = "store", type=str, dest = 'input', help='Input file')
args = parser.parse_args()

#read files
filtered_data = pd.read_csv(args.input)

#################################
##############language###########
filtered_data[['lang']] = filtered_data[['lang']].fillna('en')
filtered_data.loc[filtered_data.lang == 'English', 'lang'] = "en"
filtered_data.loc[filtered_data.lang.str.lower() == 'german', 'lang'] = "ge"
filtered_data.loc[filtered_data.lang.str.lower() == 'norwegian', 'lang'] = "no"
filtered_data.loc[filtered_data.lang.str.lower() == 'norweigian', 'lang'] = "no"
filtered_data.loc[filtered_data.lang == 'Spanish', 'lang'] = "es"
#note, implicit, change NAs to empty string
filtered_data[['notes']] = filtered_data[['notes']].fillna('')
###############################################
#########change_type and before_after##########
##change_type
filtered_data['change_type'] = filtered_data['change_type'].str.lower()
filtered_data.loc[filtered_data.change_type == 'changed_added', 'change_type'] = "added"
filtered_data.loc[filtered_data.change_type == 'constant', 'change_type'] = "unchanged"
#before_after
filtered_data['before_after'] = filtered_data['before_after'].str.lower()
filtered_data['before_after'].fillna('unchanged', inplace=True)
#lock information using both information
filtered_data.loc[(filtered_data.change_type == 'changed') & (filtered_data.before_after == 'before'), 'change_type'] = "changed_before"
filtered_data.loc[(filtered_data.change_type == 'changed') & (filtered_data.before_after == 'after'), 'change_type'] = "changed_after"
compare_ty = pd.crosstab(filtered_data.change_type, filtered_data.before_after, margins=True, margins_name="Total")
#################################
#############text_type###########
filtered_data[['position_type','boundary_type','aggregation_type','payoff_type',
'information_type','communication_type','choice_type','scope_type']] = filtered_data[['position_type',
'boundary_type','aggregation_type','payoff_type','information_type','communication_type',
'choice_type','scope_type']].fillna(0)
filtered_data[['rule_norm_strategy','reg_const']] = filtered_data[['rule_norm_strategy','reg_const']].fillna('none')
filtered_data.loc[filtered_data.reg_const == 'constitutive ','reg_const'] = 'constitutive'
filtered_data.loc[filtered_data.reg_const == 'regulatory ','reg_const'] = 'regulatory'
filtered_data.loc[filtered_data.rule_norm_strategy == 'strategy ','rule_norm_strategy'] = 'strategy'
filtered_data.loc[filtered_data.rule_norm_strategy == 'norm ','rule_norm_strategy'] = 'norm'
#################################
##############IS/Non-IS###########
filtered_data['IS'] = 1
filtered_data.loc[filtered_data.rule_norm_strategy == 'none','IS'] = 0
#################################
########code_date&timestamp######
filtered_data[['code_date','timestamp']] = filtered_data[['code_date','timestamp']].astype(str)
filtered_data['code_date'] = filtered_data.apply(lambda row: row.code_date.split('.')[0], axis=1)
filtered_data['timestamp'] = filtered_data.apply(lambda row: row.timestamp.split('.')[0][0:8], axis=1)

#################################
############UID##################
#lineID float
IDgroup = filtered_data[['lineID','textID']].drop_duplicates()
IDgroup['decimal'] = IDgroup.groupby(['lineID']).cumcount()+1
filtered_data = pd.merge(filtered_data, IDgroup, how='outer', on=['lineID', 'textID'])
filtered_data['lineID'] = filtered_data['lineID'].astype(str)
filtered_data['decimal'] = filtered_data['decimal'].astype(str)
filtered_data['lineID_float'] = filtered_data.apply(lambda row: row.lineID + '.' + row.decimal, axis=1)

#UID: each row is unique on domain+communityID+lineID+coder+coder_date+timestamp
filtered_data['UID'] = filtered_data.apply(lambda row: row.domain + '_' + row.communityID + '_' +
                       row.lineID_float + '_' + row.coder + '_' + row.code_date + '_' + row.timestamp, axis=1)
filtered_data['textID'] = filtered_data.apply(lambda row: row.domain + '_' + row.communityID + '_' +
                       row.lineID_float + '_'  + row.timestamp, axis=1)
################################
##########assert test###########
assert filtered_data.text.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.coder.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.timestamp.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.domain.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.communityID.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.lineID.isnull().sum().sum() == 0, 'warning NAs'
###############################
#######reorder the columns#####
filtered_data = filtered_data[['UID', 'domain', 'communityID', 'lineID', 'text',
                               'lang', 'ref', 'timestamp', 'change_type', 'codify_stage',
                               'coder', 'code_date', 'IS','reg_const', 'rule_norm_strategy',
                               'position_type', 'boundary_type','aggregation_type',
                               'payoff_type','information_type','communication_type',
                               'choice_type','scope_type','data_source','textID','row']]


###############################
##set the right type###########
filtered_data['change_type'] = filtered_data['change_type'].astype('category')

###############################
#########filter the dev########
filtered_data['app'] = 'remain'
filtered_data.loc[((filtered_data['row'].isin(range(1,2888)))& (filtered_data['data_source'] == 'typology2')), 'app'] = 'dev'
filtered_data.loc[((filtered_data['row'].isin(range(101,1180)))& (filtered_data['data_source'] == 'typology3')),'app'] = 'dev'
##get rid of data_source, it's redundant with codify_stage
filtered_data = filtered_data.drop(columns=['data_source','row'])

##split dataset
dev = filtered_data[filtered_data['app']== 'dev'].drop(columns = ['app'])
dev = dev.drop_duplicates()
filtered_data = filtered_data[filtered_data['app']== 'remain'].drop(columns = ['app'])
filtered_data = filtered_data.drop_duplicates()
###############################
#########output################
cwd = os.getcwd()
path = cwd + '/data/step1/'
filtered_data.to_csv (path+'filtered_typology.csv', index = False, header=True)
dev.to_csv(path+'dev_typology.csv', index = False, header=True)
