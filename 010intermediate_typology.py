import pandas as pd
import os
import numpy as np
import csv


###get excelfile path
root_typology = "./data/raw_touchedup/Typology"
foldernames= os.listdir (root_typology) # get all files' and folders' names in the current directory
foldernames.remove('.DS_Store')
xlspath = []
#Drop DANIELM
coders = ['BROWNT','RODAS','NGOS','HARTM','CHANGK','LEE','beizais','WUY','zaras','JinX',
'DELEONSILVAB','PASCALISJ','JONATHANWU','STEPHANOA','DHANOAB','YEY','DANIELM']
for folder in foldernames:
    files = os.listdir (root_typology+'/'+folder)
    files_xls = [f for f in files if f[-4:] == 'xlsx']
    xlsname = files_xls[0]
    if xlsname.split('_')[-1][:-5] in coders:
        xlspath.append(root_typology+'/'+folder+'/'+xlsname)

###read excel sheets and filter
full_dict = {}
sheets = []
count = 0
for path in xlspath:
    xls = pd.ExcelFile(path)
    sheet_names = xls.sheet_names
    sheets.extend(sheet_names)
    x = 0
    for x, sheet_name in enumerate(sheet_names, start=0):
        df = xls.parse( x )
        df['row'] = df.index
        coder_name = sheet_name.split('_')[2]
        stage_name = sheet_name.split('_')[1]
        #filter out coders who doesn't belong to this tab
        df = df[df.coder.str.lower() == coder_name.lower()]
        df.dropna(subset=['text','coder','domain','lineID','communityID','timestamp'],inplace=True)
        df['data_source'] =  stage_name
        if count > 0:
            filtered_data = filtered_data.append(df,sort=False)
        else:
            filtered_data = df
        count = count + 1

#data clean
filtered_data = filtered_data[filtered_data.coder != 'FREY']
#merge DELEONSILV into DELEONSILVAB
filtered_data.loc[filtered_data.coder == 'DELEONSILV', 'coder'] = "DELEONSILVAB"

#group on unique text
filtered_data['textID'] = filtered_data.groupby(['text','lineID','timestamp','communityID']).grouper.group_info[0]


###clean columns
#delete and rename columns
cols = [c for c in filtered_data.columns if c.lower()[:7] != 'unnamed']
filtered_data = filtered_data[cols]
filtered_data = filtered_data.rename(columns = {'before/after':'before_after'})

#drop those with NAs over 200000
filtered_data = filtered_data.drop(columns=['institution_type', 'level_of_analysis','text_type',
                            'text_translated','Translation'])

##write file into data/step1
cwd = os.getcwd()
path = cwd + '/data/step1/'
isDir= os.path.isdir(path)
if isDir == False:
    os.mkdir('data/step1')
filtered_data.to_csv (path+'intermediate_typology.csv', index = False, header=True)
