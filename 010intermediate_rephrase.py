import pandas as pd
import os
import numpy as np
import csv

##One manual change: In Fernandez onboard-change text_tweaked to text_rephrase
###get excelfile path
root_rephrase = "./data/raw_touchedup/Rephrase"
foldernames= os.listdir (root_rephrase) # get all files' and folders' names in the current directory
foldernames.remove('.DS_Store')
xlspath = []
coders = ['KNANA','RIZOA','BOBDEA','caitlyn','JEONG','CMONTES','Fernandez','CHEW',
'garlu','WANG','jendoan','HUANGJ','TSANGM','DEA','SIMST']
for folder in foldernames:
    files = os.listdir (root_rephrase+'/'+folder)
    files_xls = [f for f in files if f[-4:] == 'xlsx']
    xlsname = files_xls[0]
    if xlsname.split('_')[-1][:-5] in coders:
        xlspath.append(root_rephrase+'/'+folder+'/'+xlsname)

###read excel sheets and filter
full_dict = {}
sheets = []
count = 0
for path in xlspath:
    xls = pd.ExcelFile(path)
    sheet_names = xls.sheet_names
    sheets.extend(sheet_names)
    x = 0
    for x, sheet_name in enumerate(sheet_names, start=0):
        sheet = xls.parse( x )
        coder_name = sheet_name.split('_')[1]
        stage_name = sheet_name.split('_')[0]
        df = sheet.copy()
        df.dropna(subset=['domain','text','text_rephrase','domain','lineID','communityID','timestamp'],inplace = True)
        #make sure the coder is the same as the tab
        df['coder'].fillna('', inplace=True)
        df = df[df.coder.str.lower() == coder_name.lower()]
        df['coder'] = coder_name
        df['data_source'] =  stage_name
        df['cat'] = 'main'
        if stage_name == 'onboard':
            df.loc[df.index < 61, 'cat'] = 'dev'
            df.loc[df.index > 60, 'cat'] = 'QC'
        if count > 0:
            filtered_data = filtered_data.append(df,sort=False)
        else:
            filtered_data = df
        count = count + 1

#data_clean
filtered_data = filtered_data[filtered_data.coder != 'FREY']
#merge private World of Warcraft, Merge BOBDE into BOBDEA
filtered_data.loc[filtered_data.domain == 'private World of Warcraft', 'domain'] = "private_wow"
filtered_data.loc[filtered_data.coder == 'BOBDE', 'coder'] = "BOBDEA"


#group on unique text
#filtered_data['timestamp'] = filtered_data['timestamp'].astype(str)
##filtered_data['communityID'] = filtered_data['communityID'].astype(str)
##drop missing columns


filtered_data['textID'] = filtered_data.groupby(['domain','text','lineID','timestamp','communityID']).grouper.group_info[0]
#get rid of the "Unnamed" columns
cols = [c for c in filtered_data.columns if c.lower()[:7] != 'unnamed']
filtered_data = filtered_data[cols]
#change some col names
filtered_data = filtered_data.rename(columns = {'MASTER KEY text_rephrase':'MASTER_KEY_text_rephrase'})
filtered_data['lang'].fillna('en', inplace=True)


#fix missing data
cwd = os.getcwd()
path = cwd + '/data/step1/'
isDir= os.path.isdir(path)
if isDir == False:
    os.mkdir('data/step1')
filtered_data.to_csv (path+'intermediate_rephrase.csv', index = False, header=True)
