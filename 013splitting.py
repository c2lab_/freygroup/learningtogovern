import pandas as pd
import numpy as np
import csv
import os
import random
import argparse

##parse command
parser = argparse.ArgumentParser(description='Splitting the data')
parser.add_argument('-i',action = "store", type=str, dest = 'input', help='Input file for splitting')
args = parser.parse_args()

#read data
#full = pd.read_csv("data/step1/filtered_typology.csv")
full = pd.read_csv(args.input)
size = len(full)
training_size = round(0.75 * size)
list = full.UID
random.seed(2020)
training_UID = random.sample(set(list), training_size)
training = full[full['UID'].isin(training_UID)]
testing = full[~full['UID'].isin(training_UID)]

###############################
#########output################
cwd = os.getcwd()
path = cwd + '/data/step1/'
output = args.input.split('_')[1].split('.')[0]
training.to_csv (path + 'training_' + output + '.csv', index = False, header=True)
testing.to_csv(path + 'testing_' + output + '.csv', index = False, header=True)
