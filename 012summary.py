import pandas as pd
import numpy as np
import csv
import argparse

##parse command
parser = argparse.ArgumentParser(description='data summary')
parser.add_argument('-i',action = "store", type=str, dest = 'input', help='Input file')
args = parser.parse_args()

#read files
df = pd.read_csv(args.input)
dfname = args.input.split('_')[1][:-4]
print('###############################')
print('######',dfname, ' summary','######')
print('###############################')
#crosstab
arrange = pd.crosstab(df.coder, df.domain, margins=True, margins_name="Total")
#write file
print('coder domain crosstab:')
print(arrange)
print('###################')

#break down by datasource
datasource = df.codify_stage.value_counts(ascending=False).rename_axis('codify_stage').to_frame('counts')
print('data_source:')
print(datasource)
print('###################')


#how many times a text is being coded
df_text = df[['textID']]
coded = df_text.textID.value_counts(ascending=False).rename_axis('text').to_frame('counts')
coded_stats = coded.counts.value_counts().rename_axis('coded_times').reset_index(name='counts')
print('coded times:')
print(coded_stats)
print('###################')
