import pandas as pd
import numpy as np
import csv
import os
import argparse

##parse command
parser = argparse.ArgumentParser(description='data clean')
parser.add_argument('-i',action = "store", type=str, dest = 'input', help='Input file')
args = parser.parse_args()
#read data
filtered_data = pd.read_csv(args.input)
############
#language##
filtered_data.loc[filtered_data.lang == 'eng', 'lang'] = "en"
#filtered_data['ref'].fillna('', inplace=True)
filtered_data[['ref','notes','implicit']] = filtered_data[['ref','notes','implicit']].fillna('')
#################################
########code_date&timestamp######
filtered_data[['code_date','timestamp']] = filtered_data[['code_date','timestamp']].astype(str)
filtered_data['code_date'] = filtered_data.apply(lambda row: row.code_date.split('.')[0], axis=1)
filtered_data['timestamp'] = filtered_data.apply(lambda row: row.timestamp.split('.')[0][0:8], axis=1)
#################################
############UID##################
#lineID float
IDgroup = filtered_data[['lineID','textID']].drop_duplicates()
IDgroup['decimal'] = IDgroup.groupby(['lineID']).cumcount()+1
filtered_data = pd.merge(filtered_data, IDgroup, how='outer', on=['lineID', 'textID'])
filtered_data['lineID'] = filtered_data['lineID'].astype(str)
filtered_data['decimal'] = filtered_data['decimal'].astype(str)
filtered_data['lineID_float'] = filtered_data.apply(lambda row: row.lineID + '.' + row.decimal, axis=1)

#UID: each row is unique on domain+communityID+lineID+coder+coder_date+timestamp
filtered_data['UID'] = filtered_data.apply(lambda row: row.domain + '_' + row.communityID + '_' +
                       row.lineID_float + '_' + row.coder + '_' + row.code_date + '_' + row.timestamp, axis=1)
filtered_data['textID'] =  filtered_data.apply(lambda row: row.domain + '_' + row.communityID + '_' +
                       row.lineID_float + '_'  + row.timestamp, axis=1)



################################
##########assert test###########
assert filtered_data.text.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.coder.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.timestamp.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.domain.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.communityID.isnull().sum().sum() == 0, 'warning NAs'
assert filtered_data.lineID.isnull().sum().sum() == 0, 'warning NAs'

#sanity check
#print(filtered_data.isna().sum())
###############################
#########filter the QC########
##get rid of data_source, it's redundant with codify_stage
#filtered_data = filtered_data.drop(columns=['data_source'])
##split dataset
filtered_data.loc[filtered_data.KEY_text_rephrase.notnull(), 'cat'] = 'QC'
filtered_data.loc[filtered_data.MASTER_KEY_text_rephrase.notnull(), 'cat'] = 'QC'
filtered_data = filtered_data[['UID', 'domain', 'communityID', 'lineID', 'text',
                               'lang', 'ref', 'timestamp',  'codify_stage',
                               'coder', 'code_date','text_type','text_rephrase',
                               'implicit','context','passive_voice','data_source','textID','cat']]
filtered_data = filtered_data.drop_duplicates()

QC = filtered_data[filtered_data['cat'] == 'QC'].drop(columns = ['cat'])
dev = filtered_data[filtered_data['cat'] == 'dev'].drop(columns = ['cat'])
filtered_data = filtered_data[filtered_data['cat']== 'main'].drop(columns = ['cat'])
##
#output
cwd = os.getcwd()
path = cwd + '/data/step1/'
filtered_data.to_csv (path+'filtered_rephrase.csv', index = False, header=True)
QC.to_csv(path+'QC_rephrase.csv', index = False, header=True)
dev.to_csv(path+'dev_rephrase.csv', index = False, header=True)
