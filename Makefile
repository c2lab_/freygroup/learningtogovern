step1:
	echo merging raw data into a csv. performing some cleaning and QC
	python 010intermediate_typology.py
	python 010intermediate_rephrase.py
	python 011data_clean_typology.py -i data/step1/intermediate_typology.csv
	python 011data_clean_rephrase.py -i data/step1/intermediate_rephrase.csv
	python 012summary.py -i data/step1/filtered_typology.csv
	python 012summary.py -i data/step1/filtered_rephrase.csv
	rscript 014typology_refine.R
	python 013splitting.py -i data/step1/filtered_typology_final.csv
	python 013splitting.py -i data/step1/filtered_rephrase.csv
